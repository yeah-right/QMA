var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var methodOverride = require('method-override');
var errorHandler = require('errorhandler');

var config = require('./development.js');

module.exports = function(app) {
    var env = app.get('env');

    //setting views and view engines
    app.set('views', config.root + '/src/');
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');

    //compression middlewear and parsers
    app.use(compression());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use(methodOverride());
    app.use(cookieParser());

    app.set('appPath', path.join(config.root + '/src/'));

    if('production' === env) {
        app.use(favicon(path.join(config.root, '/src/', 'favicon.ico')));
        app.use(express.static(app.get('appPath')));
        app.use(morgan('dev'));
    }

    if ('development' === env) {
        app.use(require('connect-livereload')());
    }

    if ('development' === env || 'test' === env) {
        app.use(express.static(path.join(config.root, '.tmp')));
        app.use(express.static(app.get('appPath')));
        app.use(morgan('dev'));
        app.use(errorHandler()); // the error handler has to be last
    }
};
