'use-strict';

var path = require('path');

//Development specific configuration
//=================================
module.exports = {

    //root path of the server
    root: path.normalize(__dirname + '../../..'),

    //development port, don't think ill use env
    port: process.env.PORT || 9000,


}
