'use-strict';

var express = require('express');
var router = express.Router();
var path = require('path');

module.exports = function(app) {

    router.get('/', function(req, res) {
        res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });

    var apiBase = '/api/v1'

    app.use(apiBase + '/form', require('./api/form'));

    // All undefined assets/components or api routes should return a 404
    app.route('/:url(api|components|app|bower_components)/*')
    .get(function(req, res) {
        res.send('404');
    });

    //All other routes should redirect to index.html
    app.route('/*')
    .get(function(req, res) {
        res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });
};
