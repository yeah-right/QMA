//The Main
//Application file
//Express backend
'use-strict';

var express = require('express');
var path = require('path');
var config = require('./config/development.js');

//Setting up the server
var http = require('http');
var app = express();
var server = http.createServer(app);

//need to have an express configuration file here
require('./config/express.js')(app)
require('./routes.js')(app);

//Starting the server
function startServer() {
    server.listen(config.port, function() {
        console.log('** EXPRESS SERVER **');
        console.log('*** LISTENING ON ***');
        console.log('PORT: ' + config.port);
    })
}

setImmediate(startServer);

//Expose app
exports = module.exports = app;
