'use-strict';

//react include/require/import
import  React  from 'react';

//right side form inputs - include/require/imports
import VisitedPagePath from './formInputs/visitedPagePath.js';
import VisitedPageHost from './formInputs/visitedPageHost.js';
import UserFirstName from './formInputs/userFirstName.js';
import UserLastName from './formInputs/userLastName.js';
import UserEmail from './formInputs/userEmail.js';
import UserLogin from './formInputs/userLogin.js';
import UserOriginalReferrerDomain from './formInputs/userOriginalReferrerDomain.js';
import SessionLandingPath from './formInputs/sessionLandingPath.js';
import PageResponseTime from './formInputs/pageResponseTime.js';
import ScreenHeight from './formInputs/screenHeight.js';

let AllRows = React.createClass({
    render: function() {
        return (
            <FormRows />
        );
    }
});


//class/render of the list of rows and the add button at the bottom
//this is the exported class and where the magic happens :D
let FormRows = React.createClass({

    //setting initial state trying to make rowCount = an array with 1
    //need to add to the array with prev # +1 in order to add the key attribute
    //to each instance of the row
    //I need to get the initial state or else this.state.rowCount is undefined.
    getInitialState: function() {
        return {
            rowCount: 1,
        };
    },

    addRow: function() {
        console.log('adding row to array');
        this.setState({rowCount: this.state.rowCount + 1})
    },

    render: function() {
        var inputs = [];
        for(var i = 0; i < this.state.rowCount; i++) {
            inputs.push(i);
        }

        return (
            <div>
                {inputs.map(function(singleRow, i) {
                    return <div className="row" key={singleRow}>
                        <SearchOptionsRow />
                    </div>
                })}
                <div className="row">
                    <div className="col-md-12">
                        <button onClick={this.addRow} type="button" className="btn btn-primary add-row-button">AND</button>
                    </div>
                </div>
            </div>
        );
    }
});

//The entire row with dynamic inputs.
let SearchOptionsRow = React.createClass({

    //setting the initial state to the default option selected
    //which is the page host option
    //I HAVE NO CLUE WHY... will figure this out later
    getInitialState: function() {
        return {
            selectedOption: 'Visited Page Host'
        };
    },

    selectLeftOption: function(event) {
        console.log('on change');
        this.setState({
            selectedOption: event.target.value
        });
    },

    //somehow I need to put a .map function here to create more than one dropdown...
    //EDIT: worked by taking the rowCount and pushing it to an array will now .map them
    render: function() {

        var rightSideInputs = [ ['Visited Page Path', VisitedPagePath],
                                ['Visited Page Host', VisitedPageHost],
                                ['User Last Name', UserLastName],
                                ['User First Name', UserFirstName],
                                ['User Email', UserEmail],
                                ['User Login', UserLogin],
                                ['User Original Referrer Domain', UserOriginalReferrerDomain],
                                ['Session Landing Path', SessionLandingPath],
                                ['Page Response Time', PageResponseTime],
                                ['Screen Height', ScreenHeight] ];

        for(var i = 0; i < rightSideInputs.length; i++) {
            if(rightSideInputs[i][0] === this.state.selectedOption ) {
                console.log('MATXCH');
                this.props.matchingInput = rightSideInputs[i][1];
            }
        }
        //the for loop sets the matching input to props, I just need to figure out how
        //to make the imported input name into a jsx html element (DONE)
        //inputsAndData.push(this.props.matchingInput);
        console.log('INPUTS AND DATA');
        //console.log(inputsAndData);
        var correctInput = <this.props.matchingInput />;

        //I had to move the button out of its own custom class in order to reach the function in the class of the row
        //something weird in this framework about scopes that is more strange than vanilla js's scope problems
        return (
            <div>
                <div className="dropdown-options-left">
                    <div className="col-md-1">
                        <button type="button" className="bullet-btn">-</button>
                    </div>
                    <div className="col-md-5">
                        <select onChange={this.selectLeftOption} value={this.state.selected}>
                            <optgroup className="option-seperator">
                                <option value="Visited Page Path">Visited Page Path</option>
                                <option value="Visited Page Host">Visited Page Host</option>
                            </optgroup>
                            <optgroup className="option-seperator" label="______________________________________________________________">
                                <option value="User Last Name">User Last Name</option>
                                <option value="User Login">User Login</option>
                                <option value="User Email">User Email</option>
                                <option value="User Original Referrer Domain">User Original Referrer Domain</option>
                                <option value="Page Response Time">Page Response Time</option>
                                <option value="Session Landing Path">Session Landing Path</option>
                                <option value="Screen Height">Screen Height</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
                {correctInput}
            </div>
        );
    }
});

module.exports = AllRows;
