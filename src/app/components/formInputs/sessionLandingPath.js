'use-strict';

import React from 'react';

let SessionLandingPath = React.createClass({
    render: function() {
        return (
            <div className="input-options-right" name="SessionLandingPath">
                <div className="col-md-2 host-dropdown-right">
                    <select>
                        <option value="does not equal">does not equal</option>
                    </select>
                </div>
                <div className="col-md-4">
                    <div className="form-group">
                        <input type="text" className="form-control col-xs-4" />
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = SessionLandingPath;
