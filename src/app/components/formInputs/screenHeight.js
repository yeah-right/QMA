'use-strict';

import React from 'react';

let ScreenHeight = React.createClass({
    render: function() {
        return (
            <div className="input-options-right" name="ScreenHeight">
                <div className="screen-height">
                    <div className="col-md-1 response-time-is">
                        <button type="button" disabeled>is</button>
                    </div>
                    <div className="col-md-2 host-dropdown-right">
                        <select>
                            <option value="in between">in between</option>
                        </select>
                    </div>
                    <div className="form-group col-md-1 response-time-number-input">
                        <input type="number" className="form-control" />
                    </div>
                    <div className="col-md-1 response-time-and">
                        <button type="button" disabeled>and</button>
                    </div>
                    <div className="form-group col-md-1 response-time-number-input">
                        <input type="number" className="form-control" />
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = ScreenHeight;
