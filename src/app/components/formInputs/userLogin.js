'use-strict';

import React from 'react';

let UserLogin = React.createClass({
    render: function() {
        return (
            <div className="input-options-right" name="UserLogin">
                <div className="col-md-2 host-dropdown-right">
                    <select>
                        <option value="Contains">Contains</option>
                    </select>
                </div>
                <div className="col-md-4">
                    <div className="form-group">
                        <input type="text" className="form-control col-xs-4" placeholder="Enter login info...." />
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = UserLogin;
