'use-strict';

import React from 'react';

let UserOriginalReferrerDomain = React.createClass({
    render: function() {
        return (
            <div className="input-options-right" name="UserOriginalReferrerDomain">
                <div className="col-md-2 host-dropdown-right">
                    <select>
                        <option value="Contains">Contains</option>
                    </select>
                </div>
                <div className="col-md-4">
                    <div className="form-group">
                        <input type="text" className="form-control col-xs-4" placeholder="Enter referrer domain...." />
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = UserOriginalReferrerDomain;
