'use-strict';

import React from 'react';

let VisitedPageHost = React.createClass({
    render: function() {
        return (
            <div className="input-options-right" name="VisitedPageHost">
                <div className="col-md-2 host-dropdown-right">
                    <select>
                        <option value="Contains">Contains</option>
                    </select>
                </div>
                <div className="col-md-4">
                    <div className="form-group">
                        <input type="text" className="form-control col-xs-4" placeholder="Enter host..." />
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = VisitedPageHost;
