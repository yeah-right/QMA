'use strict';
//module includes/requires/imports
import React from 'react';

//component includes/requires/imports
import SearchHeader from'./components/searchHeader.js';
import FormRows from './components/formRows.js';

//master render class
//this is where it all comes together :D :D
let Search = React.createClass({

    submitForm: function(e) {
        e.preventDefault();
        console.log('*** EVENT BELOW ***');
        //because the Search class OWNS
        //the SearchHeader and FormRows classes, The docs say I should be able to access them
        //through this.props.children, but when I log it here it is undefined.

        $.ajax({
            type: 'POST',
            url: '/api/v1/form',
            data: {data: 'send to backend, the backend will return a 200'},
            success: function(data) {
                console.log(data);
            }.bind(this)
        });
    },


    render: function() {
        return (
            /*jshint ignore:start */
            <div className="main">
                <form onSubmit={this.submitForm}>
                    <SearchHeader />
                    <FormRows />
                </form>
            </div>
            /*jshint ignore:end */
        );
    }
});



//master render... well the only render outside of a class :D
React.render(
    /*jshint ignore:start */
    <Search />,
    /*jshint ignore:end */
    document.getElementById('app')
);
