![alt text][logo]
[logo]: http://smashingboxes.com/media/W1siZiIsIjIwMTUvMTAvMjAvMTAvNDQvNTMvNjk3L1JlYWN0XzEuMC5wbmciXV0/React%201.0.png?sha=886b9b43c826ec79 "ReactJs Logo"

Getting Started
======

##### NOTE:
- I forgot to remove .git from cloned boilerplate and also forgot to git init to create a fresh commit history
- My commit history begins with commit \#80
- ~~Backend currently not working, to see what was working before I had to recreate everything please see commit `5236e9ad`~~
    * ~~and run `gulp dev` at port 3000 locally~~
    * Backend Created and WORKING!

## Prequisites/Resources (What ya need!/What I used!)
- [Git](https://git-scm.com/) `git clone` into local dir
- [Node.js](nodejs.org) Node >=0.10.0 (Node^4.2.3 is reccommended)
- [npm](nodejs.org) npm ^2.14.7
- [Bower](bower.io) (`npm install --global bower`)
- [Ruby](https://www.ruby-lang.org) and then `gem install sass` and `gem install compass`
- [Gulpjs](http://gulpjs.com/) `npm install -g gulp`
- [Generator-React-Boilerplate](https://github.com/mitchbox/generator-react-boilerplate) Is the react boilerplate generated with yeoman that I initially started with. After getting to a certain point however, I had to sit down and create my own express server/backend and API endpoints + gulpfile to continue working on sending data to the backend.

## Development
- Run `npm install` to install server-side dependencies.
- Run `bower install` to install client-side dependencies.
- ~~Run `gulp dev` to run locally~~
    * ~~The server runs locally on localhost:3000 and will open in your default browser on successful `gulp dev` execution.~~
    * ~~Server livereload and port inside of the gulpfile~~
    * ~~I just realized that gulp was only running a development server on the front end.~~
        * ~~I now have to decide whether or not to make my own gulpfile, and write an express server on the backend, or write a simpler gulpfile, and move everything over to an express application with the backend created by the generator.~~
            * ~~Either way above, because of the problem, I don't think I am going to sleep as finishing this is very important to me.~~
- Run `gulp` in one shell and wait for green `all files` message to show then
- Run `node server\app.js` from the root of this directory to begin the express server and visit localhost:9000.
    * ~~For some reason, I am getting back an error that says empty react component. I think it may have something to do with the gulpfile, maybe I should only use gulp for sass.~~

## Timeline
- created left side (no functionality)
    * ~1 hour 30 mins
- left side add row button working (time including doc reading; had to understand scope and state)
    * ~6-7 hours (took longer than it should...)
- getting value of selected option from right side select (spent a lot of time with event.target and reading docs)
    * ~5 hours (easy once I understood how form events worked, but took longer than it should)
- changing selected left option changes the inputs to the right of it
    * ~2.5 hours
- Realizing that this boilerplate has no backend associated with it and trying several times to make a quick one, several times to rewrite the gulpfile, several times doing both, and getting nowhere.
    * ~2 hours
- Decided to remake the gulp file and create my own express backend, something I have never done before, and I am finished! (so happy I just didn't generate an application with the express generator because I learned A LOT)
    * ~3 hours
- Sending data from the front end to the back end (Don't have form data yet, working on it)
    * ~2 hours
- Trying to figure out how to:
    1. Get a list of component instances (row)
    2. Get the values of the components of the row, <input>s, <select>s, etc
    3. Set these values, or even just the list of component instances to this.props.children
    4. Access the values or this.props.children in the root (./src/app/app.js) file where the form submit button is located.
        * ~4 hours on all this and have not finished an entire number. I haven't slept and I think my lack of sleep is inhibiting the efficiency and thoroughness of my thought process.
            * Thinking about emailing the repo now, and coming back to it later today or tomorrow when I have rested some.

Project Stories and TODO
======

## Stories/Main Goals:
1. ~~Use Node + Express (backend/server) and ReactJS (front end framework)~~
2. ~~Pressing 'AND' button creates a new predicate row~~
    * ~~took a bit, had to read up on getInitialState and muting that state to map it in the render~~
3. ~~Changing predicate changes the input value possibilities as shown~~
    * ~~I assume this means that the value you select in the left dropdown row is removed from the total list~~
    * ~~Probably to avoid duplicate input values~~
    * ~~I was wrong, didn't realize how the right side values correspond to the left side inputs. Going to have to rewrite a bunch I think, but I CAN DO IT!~~
        * DID IT. Took a little bit but I got it all worked out. IT FEELS GOOD TO ACCOMPLISH THINGS!!!!
4. When you press search, the search queries should end up on the node side.
    * ~~Connect the front end with the node/express back end via $.ajaj({})~~
        * I just had to restructure and rewrite the format of my api and endpoints to conform more to best practices
    * Get the form data from all the components spread out over different files
        * Getting that array of the dynamically created rows?
    * ~~Sending the form data from front end to back end.~~
    * So far I have unique keys for each dropdown on the left which should make getting those values easy.
        * somehow use the unique keys to form an array???

## TODO:
- ~~Create the right side format~~
- ~~Move the dropdown caret to the right~~
- ~~Make dropdown value the selected option~~
- ~~add AND button to the bottom of the rows~~
- ~~make the add button work. L:JFIHSDKLJSFDNSF:DKL~~
- ~~really quick, formatting of top header/form submit button is gone, fix.~~
- ~~create one input group for the right side then work on getting value of left side select~~
- ~~Get the value of the option from the select list in order to change the right side inputs~~
- ~~get logic down for checking selected option against possible inputs (imported)~~
- ~~create the right side whose inputs correspond to values on left~~
    * ~~I JUST REALIZED THAT THE RIGHT SIDE INPUTS CORRESPOND TO THE LEFT SIDE.~~
    * ~~I should still be able to complete it, just need to think through this logic.~~
    * ~~Have an 'onChange{}' for the selected option and change the right hand column to match~~
- ~~create an entire express backend server so you don't have to `gulp dev` to only develop on the client side~~
- ~~Edit the gulpfile to ignore production-server, development-server, and get browserify and watchify inside the same gulp task.~~  
- create spacing between the options in the left row select    
- get rid of the extra margin bottom on the screen height row?  
    * It is also happening with the Page Response Time; it has to have something to do with the way I formatted it
- How do I get an array of the dynamically created components?
    * I need this list in order to get values from input fields
    * I am pretty positive I get it from this.props.children, I just don't entirely understand how the props are set and how they are accessed.
        * I am convinced that it is something that is inherently wrong with the logic in my code as being the reason why setting this.props.children is next to impossible. I may have refactored my components too much seeing as the submit form button is in the outermost component file (./src/app/app.js)
- figure out how to share the form data between components (I am not sure if this.pops.children is going to work)
    * Because I have the form submit button in its own component file, not sure if children will reach anything.
- ~~read article I found on refactoring in reactjs~~
    * ~~It was about refactoring your es5 react into es6.... lol~~
- refactor components into different files
- refactor refactor refactor

## Things I don't fully understand and need to read more about"
- Children props and what they inherit (their scope?)
- Transferring props/adding to their pre-existing values
- How to get a list of component instances...
